---
layout: markdown_page
title: University
---

## What is GitLab University

Version control will be the standard for creation of any type of media in the
future. Today it's still pretty hard to master.

GitLab University has as a goal to teach Git, GitLab and everything that relates
to that to anyone willing to listen and participate.

## Classes

Right now, there is a GitLab University class every Thursday at 5PM UTC.
To sign up, send Job a message.

In the future we want to have multiple classes:

- Lectures on new and exciting things
- Git 101
- GitLab 101
- Advanced GitLab
- Infrastructures and Enterprise

If you're interested in any of these, would like to teach in one of them or
participate or help in any way, please contact Job or submit a merge request.

### Recordings

Link to recordings here.

### Test results

Link to test results here.

## Resources

List of resources here.
